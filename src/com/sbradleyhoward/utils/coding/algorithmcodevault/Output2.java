package com.sbradleyhoward.utils.coding.algorithmcodevault;

/**
 * 	Author: S Bradley Howard (friends call me Brad)
 * 	Website:	sbradleyhoward.com (Password: showme) 
 * 
 */


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;

public class Output2 extends JFrame implements WindowListener
{
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	
	private JTextPane pane = new JTextPane();
	
	private StyledDocument doc;
	
	private Style regular;
	private Style keyword;
	private Style comment;
	private Style blockcomment;
	
	private String snippetname = null;

	private static int offset;
	private int inserts = -1;
	private int removes = 0;
	private int fontsize = 11;
	
	private final Snippet snippet;


	//	/**
	//	 * Launch the application.
	//	 */
	//	public static void main(String[] args) {
	//		EventQueue.invokeLater(new Runnable() {
	//			public void run() {
	//				try {
	//					Output2 frame = new Output2();
	//					frame.setVisible(true);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
	//		});
	//	}

	/**
	 * Create the frame.
	 */
	public Output2(Snippet snip) {

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));

		snippetname = snip.getName();
		snippet = snip;
		this.setTitle(snippetname);

		pane.setMargin(new Insets(5,10,15,10));
		pane.setBorder(BorderFactory.createTitledBorder("Algorithm Code Snippet"));

		doc = pane.getStyledDocument();
		pane.setStyledDocument(doc);


		//=========================================================================================================	


		// listens for the user changing the snippet text
		pane.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent ev) {
				inserts++;
				snippet.setChanged(true);
			}
			@Override
			public void removeUpdate(DocumentEvent ev) {
				removes++;	
				snippet.setChanged(true);
			}
			@Override
			public void changedUpdate(DocumentEvent ev) {}
		});
		
		//+++++++++++++++++

		// when user clicks inside of the Snippet viewer window, this enables editing
		pane.addMouseListener(new MouseListener()
		{
			@Override
			public void mousePressed(MouseEvent arg0) {
				pane.setEditable(true);
				pane.getCaret().setVisible(true);
			}

			// unused overrides
			@Override
			public void mouseClicked(MouseEvent click) {
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}			
		});
		
		//+++++++++++++++++

		// user adds/removes from window
		pane.addKeyListener(new KeyListener()
		{
			@Override
			public void keyTyped(KeyEvent arg0) {
				snippet.setChanged(true);
			}
			//unused overrides
			@Override
			public void keyPressed(KeyEvent key) {}
			@Override
			public void keyReleased(KeyEvent arg0) {}
		});



		//=========================================================================================================	

		Style defaultstyle = StyleContext.getDefaultStyleContext().getStyle( StyleContext.DEFAULT_STYLE);

		// formatting for the snippet viewer
		TabStop[] tabs = new TabStop[4];
		tabs[0] = new TabStop(40,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		tabs[1] = new TabStop(80,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		tabs[2] = new TabStop(120,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		tabs[3] = new TabStop(160,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		TabSet tabset = new TabSet(tabs);
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY,
				StyleConstants.TabSet, tabset);
		pane.setParagraphAttributes(aset, false);


		regular = doc.addStyle("regular", defaultstyle);
		StyleConstants.setFontSize(regular, fontsize);

		keyword = doc.addStyle("keyword", regular);
		Color keywordcolor = new Color(222,12,50);
		StyleConstants.setForeground(keyword, keywordcolor);

		comment = doc.addStyle("comment", regular);
		Color commentcolor = new Color(0,186,33);
		StyleConstants.setForeground(comment, commentcolor);

		blockcomment = doc.addStyle("blockcomment", regular);
		Color blockcolor = new Color(65,104,152);
		StyleConstants.setForeground(blockcomment, blockcolor);



		//===================================================================================

		JPanel buttonpanel = new JPanel();
		buttonpanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JButton btnSaveSnippet = new JButton("Save");
		btnSaveSnippet.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				AlgorithmCodeVault.saveSnippet(snippet,pane.getText());

				removes = 0;
				inserts = 0;
				snippet.setChanged(false);
			}
		});

		
		//+++++++++++++++++

		JButton btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Printable pages = pane.getPrintable(null, null);
				PrinterJob job = PrinterJob.getPrinterJob();
				job.setPrintable(pages);
				if( job.printDialog() == true)
				{
					try {
						job.print();
					} catch (PrinterException e) {
						out("Error in print job");
						e.printStackTrace();
					}
				}
			}
		});
		
		//+++++++++++++++++

		JButton btnToClipboard = new JButton("To Clipboard");
		btnToClipboard.setToolTipText("Copies this Snippet to the clipboard");
		btnToClipboard.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Clipboard t = Toolkit.getDefaultToolkit().getSystemClipboard();
				StringSelection data = new StringSelection(pane.getText());
				t.setContents(data, data);
			}
		});
		
		//+++++++++++++++++
		
		JButton btnRename = new JButton("Rename");
		btnRename.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String newname = "";
				String oldname = snippet.getName();
				
				while( newname.equals("") )
				{
					newname = JOptionPane.showInputDialog("Enter new name for this Snippet");
				}
				ArrayList<String> list = AlgorithmCodeVault.getList();
				if( list != null)
				{
					list.remove(oldname);
					list.add(newname);
					AlgorithmCodeVault.setList(list);
					AlgorithmCodeVault.populateList(AlgorithmCodeVault.comboSelection());
					setTitle(newname);
				}
				snippet.setName(newname);
				ConcurrentHashMap<String,Snippet> map = AlgorithmCodeVault.getMap();
				map.remove(oldname);
				map.put(newname, snippet);
			}
		});
		
		//+++++++++++++++++

		buttonpanel.add(btnSaveSnippet);
		buttonpanel.add(btnPrint);
		buttonpanel.add(btnToClipboard);
		buttonpanel.add(btnRename);

		//+++++++++++++++++

		this.getContentPane().add(buttonpanel, BorderLayout.NORTH);
		this.getContentPane().add( new JScrollPane( this.pane));

		//===================================================================================

		// puts viewer in center of screen. Offset merely "tiles" the windows
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(800,600);
		this.setLocation(dimension.width / 2 - 400+offset, dimension.height / 2 - 200+offset);
		this.setDefaultCloseOperation( WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		offset +=20;
		if( offset > 99)
			offset = 0;

		//===================================================================================

		this.addWindowListener(this);
	}

	//==============================================================================================================

	// initial opening of window changes these values, so lets reset them	
	@Override
	public void windowOpened(WindowEvent arg0) {
		removes = 0;
		inserts = 0;
		snippet.setChanged(false);
	}

	// when user closes window, checks if any changes have been made to Snippet text
	@Override
	public void windowClosing(WindowEvent arg0) {
		
		// this windows has closed to remove it from openwindow list
		AlgorithmCodeVault.updateOpenWindowList(this,true,-1);


		// ignoring changes here if user is closing program with more than one snippet open
		if( ( (AlgorithmCodeVault.exiting == true) || (snippet.isChanged() == false) ) &&
			AlgorithmCodeVault.getMap().contains(snippet) == true)
				return;

		
		// this is a new snippet OR changes made, but user is only closing THIS ONE snippet
		if(  ( AlgorithmCodeVault.getMap().contains(snippet) == false) || ( inserts > 0 || removes > 0) || (snippet.isChanged() == true) )
		{

			int result = JOptionPane.showConfirmDialog(null,
					"Save changes to: "+snippetname,
					"Question", 
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE); 

			// user wants to save if == 0
			if( result == 0)
			{
				AlgorithmCodeVault.saveSnippet(snippet, pane.getText());
				AlgorithmCodeVault.updateOpenWindowList(this,true,-1);
				removes = 0;
				inserts = 0;
				//just saved
				snippet.setChanged(false);
			}
			if( result == 1)
			{
				// discard changes
				snippet.setChanged(false);
				AlgorithmCodeVault.updateOpenWindowList(this,true,-1);
			}
		}


	}

	// unused overrides
	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {}


	//==============================================================================================================


	//==============================================================================================================


	public void addText(String string, String type)
	{
		string.concat("\n");
		if( type == "regular")
			try {
				doc.insertString(doc.getLength(), string, regular);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		else if( type == "keyword")
		{
			try {
				doc.insertString(doc.getLength(), string, keyword);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		}
		else if( type == "comment")
		{
			try {
				doc.insertString(doc.getLength(), string, comment);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		}
		else if( type == "blockcomment")
		{
			try {
				doc.insertString(doc.getLength(), string, blockcomment);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		}
	}

	//==============================================================================================================

	public void moveUp()
	{
		pane.setCaretPosition(0);
	}

	//==============================================================================================================

	public String getOutputText()
	{
		return pane.getText();
	}

	//==============================================================================================================


	private static void out(String string)
	{
		System.out.println(string);
	}

	//==============================================================================================================

	@Override
	public String getName()
	{
		return snippetname;
	}

	//==============================================================================================================

	public Snippet getSnippet()
	{
		return snippet;
	}

	//==============================================================================================================


	//==============================================================================================================









}
