package com.sbradleyhoward.utils.coding.algorithmcodevault;

import java.io.Serializable;
import java.util.Date;

public class Snippet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String snippetname;
	
	private long entrydate;
	private long lastmodified;
	
	private String text = null;
	private String language = null;
	
	private volatile boolean changed = true;

	public Snippet(String name, long date, String txt, String lang) {
		snippetname = name;
		entrydate = date;
		text = txt;
		language = lang;

	}
	
	public Snippet()
	{
		super();
	}
	
	//==============================================================================================================

	protected String getName() {
		return snippetname;
	}
	
	protected void setName(String name)
	{
		snippetname = name;
	}
	
	//==============================================================================================================

	protected Date getEntryDate() {
		if( entrydate != 0)
			return new Date(entrydate);
		else
			return null;
	}
	
	//==============================================================================================================

	protected Date getLastModifiedDate() {
		if( lastmodified != 0)
			return new Date(lastmodified);
		else
			return null;
	}
	
	//==============================================================================================================

	protected void setLastModifiedDate(long date) {
		lastmodified = date;
	}
	
	//==============================================================================================================


	protected String getLanguage() {
		return language;
	}
	
	//==============================================================================================================

	protected void setLanguage(String lang) {
		language = lang;
	}
	
	//==============================================================================================================

	protected String getSnippetText()
	{
		return text;
	}
	
	//==============================================================================================================
	
	protected void setSnippetText(String txt)
	{
		text = txt;
	}
	
	//==============================================================================================================
	
	protected void setChanged(boolean bo)
	{
		changed = bo;
	}
	
	protected boolean isChanged()
	{
		return changed;
	}
	
	//==============================================================================================================

	@Override
	public String toString() {
		return snippetname + " : " + entrydate + " : " + lastmodified + " : "
				+ language + " : " + text;
	}

}
